"""Common utility functions related to parsing tables into dictionaries."""

from typing import Dict
from typing import List
from typing import Optional


def _get_isval(line: str, delim: str, max_len: int) -> List[bool]:
    """Characterize a line based on a delimiter.

    Args:
        line: String of a whole table line, e.g. a logline with table information
        delim: Which character(s) to use as a delimiter
        max_len: Length to fill with delimiter characters at the end of a string

    Returns:
        isval: List of booleans where characters are/are not the delimiter

    Example:
        In [8]: from photon.lib.dict_utils import _get_isval
           ...:
           ...: table_lines = [
           ...:     'asdf as',
           ...:     'asd  ff',
           ...:     'asd  ff 2'
           ...: ]
           ...:
           ...: line = table_lines[0]
           ...:
           ...: _get_isval(line, ' ', len(line))
        Out[8]: [True, True, True, True, False, True, True]
    """
    isval = []
    line_len = len(line)
    for char in line:
        isval.append(char != delim)
    while len(isval) < max_len:
        isval.append(False)
    return isval


def _get_isval_list(table_lines: List[str], max_len: int, delim: str=' ') -> List[List[bool]]:
    """Characterize all table lines separately based on a delimiter.

    Args:
        table_lines: list of strings representing table rows
        max_len: Length to fill with delimiter characters at the end of a string
        delim: Which character(s) to use as a delimiter

    Returns:
        isval_list: List of boolean lists marking where characters are/are not the delimiter.

    Example:
        In [7]: from photon.lib.dict_utils import _get_isval_list
           ...:
           ...: table_lines = [
           ...:     'asdf as',
           ...:     'asd  ff',
           ...:     'asd  ff 2'
           ...: ]
           ...:
           ...: max_len = max([len(line) for line in table_lines])
           ...:
           ...: _get_isval_list(table_lines, max_len, ' ')
        Out[7]:
        [[True, True, True, True, False, True, True, False, False],
         [True, True, True, False, False, True, True, False, False],
         [True, True, True, False, False, True, True, False, True]]
    """
    isval_list = []
    for line in table_lines:
        # There is the chance that we'd get a column
        # of nothing but delimiters - we don't want that in our table.
        if not line.strip(delim):
            continue
        isval = _get_isval(line, delim, max_len)
        isval_list.append(isval)
    return isval_list


def _get_column_vals(isval_list: List[List[bool]], max_len: int) -> List[bool]:
    """Get characterization of character type for all table lines combined.

    Args:
        isval_list: List of boolean lists to parse slices from.
        max_len: max length of the character lists to parse slices from.

    Returns:
        column_vals: List of slices where non-delimiter values are.

    Example:
        In [9]: from photon.lib.dict_utils import _get_isval_list
           ...: from photon.lib.dict_utils import _get_column_vals
           ...:
           ...: table_lines = [
           ...:     'asdf as',
           ...:     'asd  ff',
           ...:     'asd  ff 2'
           ...: ]
           ...:
           ...: max_len = max([len(line) for line in table_lines])
           ...:
           ...: isval_list = _get_isval_list(table_lines, max_len, ' ')
           ...:
           ...: _get_column_vals(isval_list, max_len)
        Out[9]: [True, True, True, True, False, True, True, False, True]
    """
    column_vals = []
    for index_num in range(max_len):
        if any((index_val[index_num] for index_val in isval_list)):
            column_vals.append(True)
        else:
            column_vals.append(False)
    return column_vals


def _get_column_slices(column_vals: List[bool], min_count: int) -> List[slice]:
    """Use characterization of a table to get column indices.

    Getting column slices will go through a list of booleans and determine where
    in that list we have "breaks" that are larger than our min_count.  Breakpoints
    are determined by iterating through the string and upon finding a falsey value
    (meaning we have a delimiter character there), we will check if the next
    <min_count> items in the string are also delimiters.  If so, we'll record the
    indices of the tracked slice, and start a new one when we find another char
    that isn't our delimiter.

    e.g.

    Given the variables:
        column_vals = [0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1]
        min_count = 2

    Starting at the first index and iterating through, we'll go until index 1
    where we find a truthful value.  We'll record this index and begin "tracking"
    until we find enough non-truthful values to consider it a breakpoint.  The
    first occurrence of a "breakpoint" is at indices [4:5].  At index 4, we will
    find a non-truthy value (0) and lookahead to see if all the values ahead are
    truthy or not - if they are not all "false", we do not have a breakpoint
    because there are not enough consecutive delimiters to be considered.  We
    will record each of these as slices and return a list of them.

    Args:
        column_vals: String of a whole table line, e.g. a logline with table information
        min_count: Minimum number of delimiters that must occur in sequence to separate a column

    Returns:
        slices: List of booleans where characters are/are not the delimiter

    Example:
        In [10]: from photon.lib.dict_utils import _get_isval_list
            ...: from photon.lib.dict_utils import _get_column_vals
            ...: from photon.lib.dict_utils import _get_column_slices
            ...:
            ...: table_lines = [
            ...:     'asdf  as',
            ...:     'asd   ff',
            ...:     'asd   ff 2'
            ...: ]
            ...:
            ...: max_len = max([len(line) for line in table_lines])
            ...: isval_list = _get_isval_list(table_lines, max_len, ' ')
            ...: column_vals = _get_column_vals(isval_list, max_len)
            ...:
            ...: _get_column_slices(column_vals, 2)
        Out[10]: [slice(0, 3, None), slice(6, 7, None)]
    """
    tracking = False
    start = 0
    end = 0
    slices = []
    for index, thing in enumerate(column_vals[:-min_count]):
        if thing and not tracking:
            tracking = True
            start = index
        if tracking and not all(column_vals[index:index + min_count]):
            end = index
            tracking = False
            slices.append(slice(start, end))
    return slices


def _get_rows(table_lines: List[str], slices: List[slice], delim: str) -> List[List[str]]:
    """Parse indices from table lines into list of lists.

    Args:
        table_lines: List of table lines, e.g. loglines with table information
        slices: list of slice locations to use for columns
        delim: Delimiter that will be stripped from column values and tested against.

    Returns:
        rows: List of booleans where characters are/are not the delimiter

    Example:
        In [11]: from photon.lib.dict_utils import _get_isval_list
            ...: from photon.lib.dict_utils import _get_column_vals
            ...: from photon.lib.dict_utils import _get_column_slices
            ...: from photon.lib.dict_utils import _get_rows
            ...:
            ...: table_lines = [
            ...:     'asdf  as',
            ...:     'asd   ff',
            ...:     'asd   ff 2'
            ...: ]
            ...:
            ...: delim = ' '
            ...:
            ...: max_len = max([len(line) for line in table_lines])
            ...: isval_list = _get_isval_list(table_lines, max_len, delim)
            ...: column_vals = _get_column_vals(isval_list, max_len)
            ...: slices = _get_column_slices(column_vals, 2)
            ...:
            ...: _get_rows(table_lines, slices, delim)
        Out[11]: [['asd', 'a'], ['asd', 'f'], ['asd', 'f']]
    """
    rows = []
    for row in table_lines:
        # We don't want to use the stripped row because our slices are all based on the raw lines, but
        # we don't want to add rows that would be empty if they're only delimiters.
        if not row.strip(delim):
            continue
        # Now that we know we have values, pull them out of the unedited row, and strip delimiters from
        # the values after we've pulled them from the proper location.  This prevents us from accidental
        # slicing in the wrong location, because we use the original reference points on the original lines.
        columns = [row[slicer].strip(delim) for slicer in slices]
        rows.append(columns)
    return rows


def _get_table_dictlist(table_rows: List[List[str]], headers: Optional[List[str]]=None, has_header: bool=True) -> List[Dict[str, str]]:
    """Get list of dicts representing a table, optionally parse header information.

    Args:
        table_rows: List of table row lists(List of list of strings)
        headers: If given, manually specified headers for the columns.
        has_header: bool for if the first line of the table_lines is a header for the table.

    Returns:
        row_dicts: List of dicts with headers as keys and column values.

    Example:
        In [12]: from photon.lib.dict_utils import _get_table_dictlist
            ...:
            ...: table_rows = [['asd', 'a'], ['asd', 'f'], ['asd', 'f']]
            ...:
            ...: _get_table_dictlist(table_rows)
        Out[12]: [{'asd': 'asd', 'a': 'f'}, {'asd': 'asd', 'a': 'f'}]
    """
    if headers:
        headerdict = {index: col for index, col in enumerate(headers)}
    elif has_header:
        headerdict = {index: col for index, col in enumerate(table_rows[0])}
    else:
        headerdict = {index: str(index) for index, _ in enumerate(table_rows[0])}
    row_dicts = []
    # If has_header, we want to use the first line as headers, and will start
    # parsing values on the next line - otherwise we've been given one or will
    # use the column numbers and start at the first line.
    start_index = 1 if has_header else 0
    for row in table_rows[start_index:]:
        rowdict = {headerdict[index]: col for index, col in enumerate(row)}
        row_dicts.append(rowdict)
    return row_dicts


def parse_table_lines(table_lines: List[str], delim: str=' ', min_count: int=1, headers: Optional[List[str]]=None, has_header: bool=True) -> List[Dict[str, str]]:
    """Parse a table into a list of dicts.

    Args:
        table_lines: List of table lines, e.g. loglines with table information
        delim: Which character(s) to use as a delimiter
        min_count: Minimum number of delimiters that must occur in sequence to separate a column
        headers: If given, manually specified headers for the columns.
        has_header: bool for if the first line of the table_lines is a header for the table.

    Returns:
        row_dicts: List of dicts with headers as keys and column values.

    Example:
        In [14]: from photon.lib.dict_utils import parse_table_lines
            ...:
            ...: table_lines = [
            ...:     'asdf  as',
            ...:     'asd   ff',
            ...:     'asd   ff 2'
            ...: ]
            ...:
            ...: parse_table_lines(table_lines)
        Out[14]: [{'asdf': 'asd', 'as': 'ff'}, {'asdf': 'asd', 'as': 'ff'}]
    """
    max_len = max([len(line) for line in table_lines])
    isval_list = _get_isval_list(table_lines, max_len)
    column_vals = _get_column_vals(isval_list, max_len)
    column_indices = _get_column_slices(column_vals, min_count)
    rows = _get_rows(table_lines, column_indices, delim)
    row_dicts = _get_table_dictlist(rows, headers=headers, has_header=has_header)
    return row_dicts
